package com.youngcms.service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.youngcms.bean.LoginLog;
import com.youngcms.dao.LoginLogMapper;
@Service
public class LoginLogService extends ServiceImpl<LoginLogMapper,LoginLog> {
}

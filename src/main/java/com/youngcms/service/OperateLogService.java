package com.youngcms.service;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.youngcms.bean.OperateLog;
import com.youngcms.dao.OperateLogMapper;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fumiao
 * @since 2017-07-10
 */
@Service
public class OperateLogService extends ServiceImpl<OperateLogMapper, OperateLog> {
	
}

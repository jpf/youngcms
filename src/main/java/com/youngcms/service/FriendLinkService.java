package com.youngcms.service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.youngcms.bean.FriendLink;
import com.youngcms.dao.FriendLinkMapper;
@Service
public class FriendLinkService extends ServiceImpl<FriendLinkMapper,FriendLink> {
}

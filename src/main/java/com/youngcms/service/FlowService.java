package com.youngcms.service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.youngcms.bean.Flow;
import com.youngcms.dao.FlowMapper;
@Service
public class FlowService extends ServiceImpl<FlowMapper,Flow> {
}

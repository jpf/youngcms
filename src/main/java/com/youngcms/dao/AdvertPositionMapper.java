package com.youngcms.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.youngcms.bean.AdvertPosition;

public interface AdvertPositionMapper extends BaseMapper<AdvertPosition> {

}
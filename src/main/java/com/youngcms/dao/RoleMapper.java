package com.youngcms.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.youngcms.bean.Role;

public interface RoleMapper extends BaseMapper<Role> {
}
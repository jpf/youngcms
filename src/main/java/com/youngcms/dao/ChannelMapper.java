package com.youngcms.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.youngcms.bean.Channel;

public interface ChannelMapper extends BaseMapper<Channel>  {

	Channel selectByCode(String code);

}
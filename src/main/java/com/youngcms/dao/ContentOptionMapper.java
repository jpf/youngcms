package com.youngcms.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.youngcms.bean.ContentOption;

public interface ContentOptionMapper  extends BaseMapper<ContentOption> {

}
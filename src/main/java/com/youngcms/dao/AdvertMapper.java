package com.youngcms.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.youngcms.bean.Advert;

public interface AdvertMapper extends BaseMapper<Advert> {
    
}